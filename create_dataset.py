import os
import cv2
import numpy as np


def create_dir():
    print(os.getcwd())
    if not os.path.exists('dataset'):
        os.mkdir('dataset')


def create_gesture_dir(folder):
    os.chdir('dataset')
    if not os.path.exists(folder):
        os.mkdir(folder)
    os.chdir(folder)


def capture_image(image, count):
    image = cv2.resize(image, (50, 50), cv2.INTER_AREA)
    cv2.imwrite('{}.jpg'.format(count), image)


def create_gesture():

    cap = cv2.VideoCapture(0)
    start = False
    count = 0
    while True:
        _, frame = cap.read()
        cv2.rectangle(frame, (400,400), (100,100), (0,255,0), 0)
        crop_img = frame[100:400, 100:400]
        gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (35, 35), 0)
        _, thresh = cv2.threshold(blurred, 127, 255,
                                 cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        cv2.imshow('Thresholded', thresh)

        if cv2.waitKey(1) == ord('s'):
            start = True
        if start:
            count += 1
            capture_image(thresh, count)
        if count == 1200:
            break


def main():
    create_dir()
    folder = input('Enter the folder name: ')
    create_gesture_dir(folder)
    create_gesture()


if __name__ == '__main__':
    main()
