import os
import cv2
import numpy as np
import pickle
from sklearn.model_selection import train_test_split


def get_images_labels():
    images = []
    labels = []
    for label in os.listdir('dataset'):
        for i in range(2400):
            img = cv2.imread('./dataset/'+ label + '/{}.jpg'.format(i+1), 0)
            images.append(np.array(img, dtype=np.uint8))
            labels.append(label)

    return images, labels

def main():
    images, labels = get_images_labels()
    print('Size of images', len(images))
    print('Size of labels', len(labels))
    (Xtrain, Xtest, Ytrain, Ytest) = train_test_split(images, labels, test_size=0.3, random_state=42)
    print('Size of Xtrain', len(Xtrain))
    print('Size of Xtest', len(Xtest))
    print('Size of Ytrain', len(Ytrain))
    print('Size of Ytest', len(Ytest))

if __name__ == '__main__':
    main()
