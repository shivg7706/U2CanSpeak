import os
import cv2

COUNT = 1200

def flip_images():

	for directory in os.listdir('dataset'):
		os.chdir(directory)
		for i in range(COUNT):
			img = cv2.imread('{}.jpg'.format(i + 1), 0)
			img = cv2.flip(img, 1)
			cv2.imwrite('{}.jpg'.format(i + 1 + COUNT), img)


def main():
	flip_images()


if __name__ == '__main__':
	main()
